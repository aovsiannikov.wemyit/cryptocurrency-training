import java.util.ArrayList;
import java.util.HashMap;

// Block Chain should maintain only limited block nodes to satisfy the functions
// You should not have all the blocks added to the block chain in memory 
// as it would cause a memory overflow.

public class BlockChain {
    public static final int CUT_OFF_AGE = 10;
    
    private HashMap<byte[], BlockProp> blockChain;
    private byte[] maxHeightBlockHash;
    private TransactionPool txPool;

    /**
     * create an empty block chain with just a genesis block. Assume {@code genesisBlock} is a valid
     * block
     */
    public BlockChain(Block genesisBlock) {
        this.blockChain = new HashMap<byte[], BlockProp>();
        TxHandler txHandler = new TxHandler(new UTXOPool());
        ArrayList<Transaction> txs = genesisBlock.getTransactions();
        txHandler.handleTxs(txs.toArray(new Transaction[txs.size()]));
        UTXOPool utxoPool = txHandler.getUTXOPool();
        addTxToUTXOPool(genesisBlock.getCoinbase(), utxoPool);

        this.blockChain.put(
            genesisBlock.getHash(), 
            new BlockProp(genesisBlock, 1, utxoPool)
        );        
        this.maxHeightBlockHash = genesisBlock.getHash();
        this.txPool = new TransactionPool();
    }

    private class BlockProp {
        public Block block;
        public int heigh;
        public UTXOPool utxoPool;

        public BlockProp(Block block, int heigh, UTXOPool utxoPool) {
            this.block = block;
            this.heigh = heigh;
            this.utxoPool = utxoPool;
        }
    }

    /** Get the maximum height block */
    public Block getMaxHeightBlock() {
        return this.blockChain.get(this.maxHeightBlockHash).block;
    }

    private void addTxToUTXOPool(Transaction tx, UTXOPool utxoPool) {
            ArrayList<Transaction.Output> outputs = tx.getOutputs();
            for (Transaction.Output output: outputs) {
                UTXO utxo = new UTXO(tx.getHash(), outputs.indexOf(output));
                utxoPool.addUTXO(utxo, output);
            }
        }

    /** Get the UTXOPool for mining a new block on top of max height block */
    public UTXOPool getMaxHeightUTXOPool() {
        return this.blockChain.get(this.maxHeightBlockHash).utxoPool;
    }

    /** Get the transaction pool to mine a new block */
    public TransactionPool getTransactionPool() {
        return this.txPool;
    }

    /**
     * Add {@code block} to the block chain if it is valid. For validity, all transactions should be
     * valid and block should be at {@code height > (maxHeight - CUT_OFF_AGE)}.
     * 
     * <p>
     * For example, you can try creating a new block over the genesis block (block height 2) if the
     * block chain height is {@code <=
     * CUT_OFF_AGE + 1}. As soon as {@code height > CUT_OFF_AGE + 1}, you cannot create a new block
     * at height 2.
     * 
     * @return true if block is successfully added
     */
    public boolean addBlock(Block block) {            
        byte[] prevBlockHash = block.getPrevBlockHash();
        
        if (!this.blockChain.containsKey(prevBlockHash)) {
            return false;
        }

        BlockProp prevBlockPr = this.blockChain.get(prevBlockHash);
        int maxBlockHeight = this.blockChain.get(this.maxHeightBlockHash).heigh;
        if (prevBlockPr.heigh < maxBlockHeight - CUT_OFF_AGE) {
            return false;
        }
 
        ArrayList<Transaction> blockTxs = block.getTransactions();
        TxHandler txHandler = new TxHandler(prevBlockPr.utxoPool);
        Transaction[] validTxs = txHandler.handleTxs(blockTxs.toArray(new Transaction[blockTxs.size()]));
        if (validTxs.length != blockTxs.size())
            return false;

        UTXOPool utxoPool = txHandler.getUTXOPool();
        addTxToUTXOPool(block.getCoinbase(), utxoPool);

        BlockProp blockPr = new BlockProp(block, prevBlockPr.heigh + 1, utxoPool);

        this.blockChain.put(block.getHash(), blockPr);

        if (prevBlockPr.heigh + 1 > maxBlockHeight) {
            this.maxHeightBlockHash = block.getHash();
        }

        return true;   
    }

    /** Add a transaction to the transaction pool */
    public void addTransaction(Transaction tx) {
        this.txPool.addTransaction(tx);
    }
}