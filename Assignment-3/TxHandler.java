import java.util.ArrayList;

public class TxHandler {

    private UTXOPool utxoPool;

    /**
     * Creates a public ledger whose current UTXOPool (collection of unspent transaction outputs) is
     * {@code utxoPool}. This should make a copy of utxoPool by using the UTXOPool(UTXOPool uPool)
     * constructor.
     */
    public TxHandler(UTXOPool utxoPool) {
        this.utxoPool = new UTXOPool(utxoPool);
    }

    public UTXOPool getUTXOPool() {
        return this.utxoPool;
    }

    /**
     * @return true if:
     * (1) all outputs claimed by {@code tx} are in the current UTXO pool, 
     * (2) the signatures on each input of {@code tx} are valid, 
     * (3) no UTXO is claimed multiple times by {@code tx},
     * (4) all of {@code tx}s output values are non-negative, and
     * (5) the sum of {@code tx}s input values is greater than or equal to the sum of its output
     *     values; and false otherwise.
     */
    public boolean isValidTx(Transaction tx) {
        ArrayList<Transaction.Input> inputs = tx.getInputs();
        ArrayList<UTXO> claimedUTXO = new ArrayList<UTXO>();
        double inputSum = 0.0, outputSum = 0.0;

        for (Transaction.Input input: inputs) {
            UTXO utxo = new UTXO(input.prevTxHash, input.outputIndex);                
            Transaction.Output prevOutput = this.utxoPool.getTxOutput(utxo);

            // output claimed by tx is in the current UTXO pool
            if (!this.utxoPool.contains(utxo)) 
                return false;

            // the signatures on each input of tx are valid
            if (!Crypto.verifySignature(
                    prevOutput.address,
                    tx.getRawDataToSign(inputs.indexOf(input)),
                    input.signature)
            )
                return false;

            // UTXO isn't claimed multiple times by tx
            if (claimedUTXO.contains(utxo)) {
                return false;
            } else {
                claimedUTXO.add(utxo);
            }
            
            inputSum += prevOutput.value;            
        }

        for (Transaction.Output output: tx.getOutputs()) {
            // tx output value are non-negative
            if (output.value < 0)
                return false;

            outputSum += output.value;
        }

        // the sum of tx-s input values is greater than or equal to the sum of its output values
        if (inputSum < outputSum)
            return false;


        return true;
    }

    /**
     * Handles each epoch by receiving an unordered array of proposed transactions, checking each
     * transaction for correctness, returning a mutually valid array of accepted transactions, and
     * updating the current UTXO pool as appropriate.
     */
    public Transaction[] handleTxs(Transaction[] possibleTxs) {
        ArrayList<Transaction> transactions = new ArrayList<Transaction>();

        for (Transaction tx: possibleTxs) {
            if (isValidTx(tx)) {
                for (Transaction.Input input: tx.getInputs()) {
                    UTXO utxo = new UTXO(input.prevTxHash, input.outputIndex);
                    this.utxoPool.removeUTXO(utxo);
                }
                ArrayList<Transaction.Output> outputs = tx.getOutputs();
                for (Transaction.Output output: outputs) {
                    UTXO utxo = new UTXO(tx.getHash(), outputs.indexOf(output));
                    this.utxoPool.addUTXO(utxo, output);
                }
                transactions.add(tx);
            }
        }

        return transactions.toArray(new Transaction[transactions.size()]);
    }

}
